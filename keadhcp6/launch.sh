#!/bin/ash
route del default
route add default gw 10.99.0.2

ip -6 route del default
ip -6 route add default via 2001:db8:4::10

echo "disabling checksum offload"
ethtool --offload eth0 rx off tx off

while true; do
  rm -rf /tmp/kea-leases6.csv
  kea-dhcp6 -c /etc/kea.json 
  echo "restarting kea-dhcp6 in 5 seconds ..."
  sleep 5
done
