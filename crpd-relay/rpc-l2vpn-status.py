#!/usr/bin/python

import sys
from sys import argv
from jnpr.junos import Device

scriptname = sys.argv[0]
rtinstance = "l2vpn-" + str(sys.argv[2])

print'<output>'
print 'test test test'
print'</output>'

dev = Device(gather_facts=False)
dev.open()
data = dev.rpc.get_l2vpn_connection_information(instance=str(rtinstance), extensive=True)

connstatus = data.findtext('./instance/reference-site/connection/connection-status')
remotesite = data.findtext('./instance/reference-site/connection/connection-id')
remotepe = data.findtext('./instance/reference-site/connection/remote-pe')
instance = data.findtext('./instance/instance-name')
localsite = data.findtext('./instance/reference-site/local-site-id')
l2vpnint = data.findtext('./instance/reference-site/interface/interface-name')
inlabel = data.findtext('./instance/reference-site/connection/inbound-label')
outlabel = data.findtext('./instance/reference-site/connection/outbound-label')

print'<output>'
print "L2VPN Status for Customer " + str(sys.argv[2])
print "****************************"
print "L2VPN Connection : " + connstatus
print "Remote Site      : " + remotesite
print "Remote PE        : " + remotepe
print "Routing Instance : " + instance
print "Local Site       : " + localsite
print "Incoming Label   : " + inlabel
print "Outgoing Label   : " + outlabel
print'</output>'

