#!/bin/bash

# .env is expected to contain the following secrets:
# cat .env
# CONTAINER_REGISTRY=https://registry-1.docker.io/
# CONTAINER_REGISTRY=https://hub.juniper.net
# CONTAINER_REGISTRY_USERNAME=<username>
# CONTAINER_REGISTRY_PASSWORD=<password>
# CONTAINER_REGISTRY_EMAIL=<email>
#

export $(cat .env | grep -v '^#' | awk '/=/ {print $1}')

kubectl delete secret regcred 2>/dev/null

kubectl -n kube-system create secret docker-registry regcred \
    --docker-server=$CONTAINER_REGISTRY \
    --docker-username=$CONTAINER_REGISTRY_USERNAME \
    --docker-password=$CONTAINER_REGISTRY_PASSWORD \
    --docker-email=$CONTAINER_REGISTRY_EMAIL

kubectl create secret docker-registry regcred \
    --docker-server=$CONTAINER_REGISTRY \
    --docker-username=$CONTAINER_REGISTRY_USERNAME \
    --docker-password=$CONTAINER_REGISTRY_PASSWORD \
    --docker-email=$CONTAINER_REGISTRY_EMAIL

echo "adding image pull secret to kube-system service account ..."

kubectl -n kube-system patch serviceaccount default -p '{"imagePullSecrets": [{"name": "regcred"}]}'
