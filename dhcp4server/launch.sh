#!/bin/ash
route del default
route add default gw 10.99.0.10

echo "disabling checksum offload"
ethtool --offload eth0 rx off tx off

touch /var/lib/dhcp/dhcpd.leases

while true; do
  /usr/sbin/dhcpd -d
  echo "restarting dhcpd in 5 seconds ..."
  sleep 5
done
