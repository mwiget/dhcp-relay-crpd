#!/bin/bash

echo ""
lspci -D |grep Mellanox
echo ""
for pf in eno2 eno3 eno4 enp6s0f0np0 enp6s0f1np1; do
  echo -n "$pf sriov_numvfs "
  sudo cat /sys/class/net/$pf/device/sriov_numvfs 
done
echo ""
ip link show dev eno2
echo ""
ip link show dev eno3
echo ""
ip link show dev eno4
echo ""
ip link show dev enp6s0f0np0
echo ""
ip link show dev enp6s0f1np1
echo ""
ifconfig eno3v0
ifconfig eno4v0
ifconfig enp6s0f1np1v0
echo ""
