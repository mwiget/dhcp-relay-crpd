# TODO

- mount /config and compare extracted helper settings with initial extractions (file comparison), use it as health check to terminate. If no config found, sleep 5 secs before termination.


# Ideas

- write our own dhcp relay for XDP? Overkill IMHO
- maybe use dhcp-helper instead of isc, no need for packet filter and sockets. But likely less used.
- Ubuntu has dhcp-helper:
http://manpages.ubuntu.com/manpages/xenial/man8/dhcp-helper.8.html
"Dhcp-helper requires a 2.2 or later Linux kernel. The "Linux packet  filter"  and  "packet socket"  facilities  are  not required, which is the chief advantage of this software over the ISC DHCP relay daemon."

# Discarded ideas

- Use pyez from sidecar, but that either requires netconf over ssh config or access to /usr/bin/netconf (which calls /usr/sbin/cli). Simply sharing /var/run isn't sufficient. Seems overkill.

