all:

build:
	docker-compose -f docker-compose-server.yml build
	docker-compose -f docker-compose-clients.yml build
	docker-compose build

up:
	docker-compose up -d

clients:
	sudo ifconfig enp6s0f0np0 up
	sudo ifconfig eno1 up
	sudo ifconfig eno2 up
	sudo ifconfig eno3 up
	sudo ifconfig eno4 up
	docker-compose -f docker-compose-clients.yml up -d

server:
	sudo ifconfig enp1s0f1np1 up
	docker-compose -f docker-compose-server.yml up -d

ps:
	docker-compose -f docker-compose-server.yml ps
	docker-compose -f docker-compose-clients.yml ps

crpd1: crpd1shell

crpd1shell:
	docker-compose -f docker-compose-server.yml exec crpd1 bash

down:
	docker-compose down
	docker-compose -f docker-compose-server.yml down
	docker-compose -f docker-compose-clients.yml down --remove-orphans
