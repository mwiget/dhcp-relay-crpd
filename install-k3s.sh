#!/bin/bash

SECONDS=0
echo "installing k3s master locally ..."

export INSTALL_K3S_EXEC=$(cat <<EOF
--flannel-backend=none
--no-flannel
--no-deploy servicelb
--no-deploy traefik
--write-kubeconfig-mode "0644"
EOF
)

echo with the following settings:
echo $INSTALL_K3S_EXEC
echo ""
curl -sfL https://get.k3s.io | sh -

echo ""
kubectl get nodes -o wide
echo ""

./add-docker-registry-credentials.sh

echo ""
echo "installing host-local IPAM from containernetworking/plugins ..."
sudo mkdir -p /opt/cni/bin
(cd plugins; ./build_linux.sh && sudo cp bin/host-local /opt/cni/bin/)

echo "installing cilium ..."
#kubectl create -f https://raw.githubusercontent.com/cilium/cilium/1.8.3/install/kubernetes/quick-install.yaml
kubectl create -f cilium-quick-install.yaml

echo "installing multus ..."
kubectl create -f multus-daemonset-cilium.yml
#kubectl create -f https://raw.githubusercontent.com/intel/multus-cni/master/images/multus-daemonset.yml

echo ""
echo "labelling master node as workers ..."
kubectl label nodes $(hostname) worker=true

echo ""
kubectl get nodes -o wide
kubectl get pods --all-namespaces -o wide
echo ""

echo ""
echo "adding SR-IOV configMap ..."
kubectl create -f sriov-configMap.yaml

echo ""
echo "deploy SR-IOV network device plugin DaemonSet ..."
kubectl create -f sriovdp-daemonset.yaml  

echo ""
echo "create SR-IOV network CRD ..."
kubectl create -f sriov-crd1.yaml
kubectl create -f sriov-crd-ru1.yaml
kubectl create -f sriov-crd-ru2.yaml
kubectl create -f sriov-crd-ru3.yaml
kubectl create -f sriov-crd-ru4.yaml

echo ""
echo "create crpd configmap ..."
kubectl create configmap crpd2-config --from-file juniper.conf=crpd2.conf --from-literal dhcp-server=10.99.0.100 --from-literal dhcp6-server="2001:db8:4::1000"
kubectl -n kube-system create configmap crpd-configs --from-file ryzen1.conf --from-literal dhcp-server=10.99.0.100 --from-literal dhcp6-server="2001:db8:4::1000" 

echo ""
echo " installing crpd-daemonset ..."
kubectl apply -f crpd-daemonset.yaml
#kubectl apply -f crpd2.yaml

echo ""
echo "fix vlans on eno3v0 and eno4v0"
sudo ip link set eno3 vf 0 vlan 2003
sudo ip link set eno4 vf 0 vlan 2004

echo ""
echo "Completed in $SECONDS seconds"
