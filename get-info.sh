#!/bin/bash

echo ""
kubectl get node -A -o wide
echo ""
kubectl get node $(hostname) -o json| jq '.status.allocatable'
echo ""
kubectl get nodes -o jsonpath='{range .items[*]}{.metadata.name}{" "}{.spec.podCIDR}{" "}{.status.addresses[?(@.type=="InternalIP")].address}{"\n"}{end}'
echo ""
kubectl get pods -A -o wide
echo ""
kubectl get pods -A -o=custom-columns=NameSpace:.metadata.namespace,NAME:.metadata.name,CONTAINERS:.spec.containers[*].name
echo ""
kubectl exec crpd2 -c crpd -- cli show version
echo ""
kubectl exec crpd2 -c crpd -- cli show isis route
echo ""
pod=$(kubectl -n kube-system get pods -o wide |grep crpd- | awk '{print $1}')
kubectl -n kube-system exec $pod -c crpd -- cli show version
echo ""
kubectl -n kube-system exec $pod -c crpd -- cli show isis route
