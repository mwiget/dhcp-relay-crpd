# openconfig yang for dhcp relay

Dockerfile used to augment cRPD with the yang model. 
YANG files copied from https://github.com/openconfig/public

then the container crpd:latest understands the following configuration:

```
set openconfig-relay-agent:relay-agent dhcp config enable-relay-agent true
set openconfig-relay-agent:relay-agent dhcp agent-information-option config enable true
set openconfig-relay-agent:relay-agent dhcp interfaces interface all config helper-address 10.99.0.100
set openconfig-relay-agent:relay-agent dhcpv6 interfaces interface all config helper-address 2001:db8:4::1000
set system schema openconfig unhide
```

```
$ docker exec crpd cli show conf

## Last commit: 2021-04-25 09:07:16 UTC by root
version 20210304.225144.7_builder.r1175195;
system {
    root-authentication {
        encrypted-password "$..........."; ## SECRET-DATA
    }
    schema {
      openconfig {
        unhide;
      }
    }
}
openconfig-relay-agent:relay-agent {
    dhcp {
        config {
            enable-relay-agent true;
        }
        agent-information-option {
            config {
                enable true;
            }
        }
        interfaces {
            interface all {
                config {
                    helper-address 10.99.0.100;
                }
            }
        }
    }
    dhcpv6 {
        interfaces {
            interface all {
                config {
                    helper-address 2001:db8:4::1000;
                }
            }
        }
    }
}
```

