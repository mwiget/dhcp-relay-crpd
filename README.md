# dhcp-relay-crpd

Prototype to add dhcp-relay functionality to cRPD running in a kubernetes Pod.

## Lab setup

3 physical servers used, interconnected via physical 1GE and 10GE links:

```
+---------------+                        +------------+                         +-----------+
|    ryzen2     |                        |   ryzen1   |                         |   rog1    |
|RU1 enp6s0f0np0|===(ran-mp vlan 100)====|enp6s0f0np0 |                         |           |
|RU2        eno2|===(ran-mp vlan 2002)===|eno2        |                         |           |
|RU3        eno3|===(ran-mp vlan 2003)===|eno3  DHCP  |                         |   DHCP    |
|RU4        eno4|===(ran-mp vlan 2004)===|eno4  RELAY |                         |  SERVER   |
|               |                        |            |                         |           |
|               |                        | enp6s0f0np1|===(ran-mgmt vlan 200)===|enp1s0f0np1|
+---------------+                        +------------+                         +-----------+
   Radio-Units                               DU Node                             Server Edge
```

ryzen2 and rog1 are supporting servers that deploy RU clients (basically dhcp clients) and
dhcp server.

A logical view is shown here:

```

+-----+                 +----------------+
| RU1 |--- vlan 100  ---| net1   crpd2   |
+-----+                 |      POD       |
                        |   dhcprelay    |                +--------------+
+-----+                 |                |                |              |
| RU2 |--- vlan 2002 ---| net2 eth0 net3 |--- vlan 200 ---| eth0         |
+-----+                 +----------------+                |              |   +-------------+
                                                          |    crpd1     |   | dhcp4server |
+-----+                 +----------------+                |              |   |             |
| RU3 |--- vlan 2003 ---|eno3v0  crpd    |                |         eth2 |---| eth0        |
+-----+                 |  kube-system   |                |   10.99.0.10 |   | 10.99.0.100 |
                        |   dhcprelay    |                |              |   +-------------+
+-----+                 |  enp6s0f1np0v0 |--- vlan 201 ---| eth1         |
| RU4 |--- vlan 2004 ---|eno4v0          |                |              |
+-----+                 +----------------+                +--------------+

```


docker-compose.yml covers the simulated RU (mgmt plane with dhcp client), plus dhcp servers and cRPD on the server side.

kubernetes and kubectl manifests are used to simulate cRPD with dhcp relay.

```
$ ./get-info.sh

NAME     STATUS   ROLES                  AGE    VERSION        INTERNAL-IP    EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
ryzen1   Ready    control-plane,master   3d1h   v1.20.6+k3s1   192.168.0.51   <none>        Ubuntu 20.04.2 LTS   5.8.0-50-generic   containerd://1.4.4-k3s1

{
  "cpu": "16",
  "ephemeral-storage": "199789251223",
  "hugepages-1Gi": "4Gi",
  "hugepages-2Mi": "2Gi",
  "intel.com/i350_sriov_eno1": "4",
  "intel.com/i350_sriov_eno2": "4",
  "intel.com/i350_sriov_eno3": "4",
  "intel.com/i350_sriov_eno4": "4",
  "intel.com/mlnx_sriov_enp6s0f0": "4",
  "intel.com/mlnx_sriov_enp6s0f1": "4",
  "memory": "59434900Ki",
  "pods": "110"
}

ryzen1 10.42.0.0/24 192.168.0.51

NAMESPACE     NAME                                      READY   STATUS    RESTARTS   AGE    IP             NODE     NOMINATED NODE   READINESS GATES
kube-system   cilium-operator-7f4dc846b6-sc4nk          0/1     Pending   0          3d1h   <none>         <none>   <none>           <none>
kube-system   cilium-operator-7f4dc846b6-69gnt          1/1     Running   0          3d1h   192.168.0.51   ryzen1   <none>           <none>
kube-system   kube-sriov-device-plugin-amd64-6glvd      1/1     Running   0          3d1h   192.168.0.51   ryzen1   <none>           <none>
kube-system   kube-multus-ds-amd64-bg2nw                1/1     Running   0          3d1h   192.168.0.51   ryzen1   <none>           <none>
kube-system   local-path-provisioner-5ff76fc89d-lcp6f   1/1     Running   0          3d1h   10.0.0.192     ryzen1   <none>           <none>
kube-system   coredns-854c77959c-q558f                  1/1     Running   0          3d1h   10.0.0.108     ryzen1   <none>           <none>
kube-system   metrics-server-86cbb8457f-8r98k           1/1     Running   0          3d1h   10.0.0.233     ryzen1   <none>           <none>
kube-system   cilium-psxqt                              1/1     Running   0          3d1h   192.168.0.51   ryzen1   <none>           <none>
default       crpd2                                     2/2     Running   0          76m    10.0.0.193     ryzen1   <none>           <none>
kube-system   crpd-5qqgp                                2/2     Running   0          76m    192.168.0.51   ryzen1   <none>           <none>

Hostname: crpd2
Model: cRPD
Junos: 21.1R1.11
cRPD package version : 21.1R1.11 built by builder on 2021-03-18 19:26:08 UTC

 IS-IS routing table             Current version: L1: 9 L2: 12
IPv4/IPv6 Routes
----------------
Prefix             L Version   Metric Type Interface       NH   Via                 Backup Score
10.0.0.162/32      2      12       30 int  net3            IPV4 a620f093d338       
10.2.0.0/24        2      12       20 int  net3            IPV4 a620f093d338       
10.99.0.0/24       2      12       20 int  net3            IPV4 a620f093d338       
172.16.13.0/24     2      12       30 int  net3            IPV4 a620f093d338       
172.16.14.0/24     2      12       30 int  net3            IPV4 a620f093d338       
192.168.0.0/24     2      12       30 int  net3            IPV4 a620f093d338       
2001:db8:4::/64    2      12       20 int  net3            IPV6 a620f093d338       
2001:db8:200::/64  2      12       20 int  net3            IPV6 a620f093d338       
2001:db8:201::/64  2      12       20 int  net3            IPV6 a620f093d338       
2a02:168:5f67::/64 2      12       30 int  net3            IPV6 a620f093d338       

Hostname: ryzen1
Model: cRPD
Junos: 21.1R1.11
cRPD package version : 21.1R1.11 built by builder on 2021-03-18 19:26:08 UTC

 IS-IS routing table             Current version: L1: 11 L2: 17
IPv4/IPv6 Routes
----------------
Prefix             L Version   Metric Type Interface       NH   Via                 Backup Score
10.0.0.193/32      2      17       30 int  enp6s0f1np0v0   IPV4 a620f093d338       
10.1.0.0/24        2      17       20 int  enp6s0f1np0v0   IPV4 a620f093d338       
10.99.0.0/24       2      17       20 int  enp6s0f1np0v0   IPV4 a620f093d338       
172.16.11.0/24     2      17       30 int  enp6s0f1np0v0   IPV4 a620f093d338       
172.16.12.0/24     2      17       30 int  enp6s0f1np0v0   IPV4 a620f093d338       
2001:db8:4::/64    2      17       20 int  enp6s0f1np0v0   IPV6 a620f093d338       
2001:db8:200::/64  2      17       20 int  enp6s0f1np0v0   IPV6 a620f093d338       
```

